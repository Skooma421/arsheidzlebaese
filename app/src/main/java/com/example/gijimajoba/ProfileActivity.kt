package com.example.gijimajoba

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import com.google.firebase.auth.FirebaseAuth

class ProfileActivity : AppCompatActivity() {
    private lateinit var buttonLogout: Button
    private lateinit var buttonPasswordChange: Button
    private lateinit var textView: TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        init()
        registerListener()
        textView.text = FirebaseAuth.getInstance().currentUser?.uid
    }

    private fun init() {
        buttonPasswordChange = findViewById(R.id.buttonPasswordChange)
        buttonLogout = findViewById(R.id.buttonLogut)
        textView = findViewById(R.id.textView)
    }
    private fun registerListener(){
        buttonLogout.setOnClickListener {
            FirebaseAuth.getInstance().signOut()
            startActivity(Intent(this, loginActivity::class.java))
            finish()
        }
        buttonPasswordChange.setOnClickListener{
            startActivity(Intent(this,PassChangeActivity::class.java))
        }
    }
}