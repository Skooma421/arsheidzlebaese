package com.example.gijimajoba

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class PassChangeActivity : AppCompatActivity() {

    private lateinit var editTextNewPassword:EditText
    private lateinit var buttonChangePassword:EditText
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pass_change)
    }
    private fun init(){
        editTextNewPassword = findViewById(R.id.editTextNewPassword)
        buttonChangePassword = findViewById(R.id.buttonChangePassword)
    }
    private fun registerlisteners(){
        buttonChangePassword.setOnClickListener{
            val newPassword = editTextNewPassword.text.toString()
            if(newPassword.isEmpty()||newPassword.length<7 ){
                Toast.makeText(this,"invalid password", Toast.LENGTH_SHORT).show()
                return@setOnClickListener


            }
            FirebaseAuth.getInstance().currentUser?.updatePassword(newPassword)?.addOnCompleteListener{
                task -> if(task.isSuccessful){
                    Toast.makeText(this,"Password changed succesfully",Toast.LENGTH_SHORT).show()

            }else{
                Toast.makeText(this,"Password change unsuccessful", Toast.LENGTH_SHORT).show()
            }
            }
        }
    }
}